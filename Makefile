.phony: all
all:
	xelatex cv.tex > resume.log
	make clean

.phony: clean
clean:
	rm *.out *.aux *.log -f
